import Repository.JDBC.CazCaritabilJdbcRepository;
import Repository.JDBC.DonatieJdbcRepository;
import Repository.JDBC.DonatorJdbcRepository;
import Repository.JDBC.VoluntarJdbcRepository;
import Service.IServerService;
import donatii.server.StartServerServiceImpl;
import utils.AbstractServer;
import utils.RpcConcurrentServer;
import utils.ServerException;

import java.io.IOException;
import java.util.Properties;

public class StartRpcServer {
    private static int defaultPort=55555;
    public static void main(String[] args) {
        // UserRepository userRepo=new UserRepositoryMock();
        Properties serverProps=new Properties();
        try {
            serverProps.load(StartRpcServer.class.getResourceAsStream("/my_server.properties"));
            System.out.println("Server properties set. ");
            serverProps.list(System.out);
        } catch (IOException e) {
            System.err.println("Cannot find chatserver.properties "+e);
            return;
        }

        ///
        DonatieJdbcRepository donRepo = new DonatieJdbcRepository(new CazCaritabilJdbcRepository(serverProps), new DonatorJdbcRepository(serverProps), serverProps);
        VoluntarJdbcRepository volRepo=new VoluntarJdbcRepository(serverProps);
        IServerService serverImpl=new StartServerServiceImpl(donRepo, volRepo);
        ///

        int serverPort=defaultPort;
        try {
            serverPort = Integer.parseInt(serverProps.getProperty("my_server.port"));
        }catch (NumberFormatException nef){
            System.err.println("Wrong  Port Number "+nef.getMessage());
            System.err.println("Using default port "+defaultPort);
        }
        System.out.println("Starting server on port: "+serverPort);

        ///
        AbstractServer server = new RpcConcurrentServer(serverPort, serverImpl);
        try {
            server.start();
        } catch (ServerException e) {
            System.err.println("Error starting the server" + e.getMessage());
        }
    }
}
