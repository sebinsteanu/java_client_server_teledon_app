package donatii.server;

import Domain.CazCaritabil;
import Domain.Donator;
import Domain.Voluntar;
import Repository.IDonatieRepo;
import Repository.JDBC.DonatieJdbcRepository;
import Repository.JDBC.IDonatorRepo;
import Repository.JDBC.VoluntarJdbcRepository;
import Service.IClientService;
import Service.IServerService;
import Service.ServiceException;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by sebi on 3/28/2017.
 */
public class StartServerServiceImpl implements IServerService {

    private VoluntarJdbcRepository volRepository;
    private IDonatieRepo repositoryDonatie;
    private Map<String, IClientService> loggedClients;

    public StartServerServiceImpl(IDonatieRepo repositoryDonatie, VoluntarJdbcRepository valRepo) {
        this.volRepository = valRepo;
        this.repositoryDonatie = repositoryDonatie;
        this.loggedClients = new ConcurrentHashMap<>();
    }

    @Override
    public void makeDonation(CazCaritabil cazCaritabil, Donator donator, double suma) {
        CazCaritabil caz = repositoryDonatie.addDonation(cazCaritabil,donator, suma);
        ExecutorService executor= Executors.newFixedThreadPool(5);
        executor.execute(() -> {
        for (IClientService cli: loggedClients.values()){
            try {
                cli.updateDonatii(caz);
            }
            catch (ServiceException ex){
                System.out.println("Update error" + ex.getMessage());
            }
        }});
        executor.shutdown();
    }

    @Override
    public Iterable<Donator> getFilteredDonors(String startLetters) throws ServiceException {return repositoryDonatie.findDonators(startLetters);}

    @Override
    public Iterable<CazCaritabil> getAllCazCaritabils() {return repositoryDonatie.findAllCharityCases();}

    @Override
    public boolean login(Voluntar user, IClientService clientService) throws ServiceException{
        Voluntar vol = volRepository.findOne(user.getUsername());
        if(vol!= null){
            if(loggedClients.get(user.getUsername())!=null)
                throw new ServiceException("User already logged in!");
            loggedClients.put(user.getUsername(), clientService);
            return true;
        }
        return false;
    }

    @Override
    public void logOut(Voluntar user, IClientService clientService) throws ServiceException {
        IClientService localClient = loggedClients.remove(user.getUsername());
        System.out.println("User logout "+user);
        if (localClient==null)
            throw new ServiceException("User "+user.getId()+" is not logged in.");
    }
}
