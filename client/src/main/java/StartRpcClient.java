import Service.IServerService;
import UI.ControllerDonatii;
import UI.LoginWindow;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import rpcProtocol.ServerRpcProxy;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by sebi on 3/31/2017.
 */

public class StartRpcClient extends Application {
    private static Stage pStage;
    private static ControllerDonatii _controller;

    private static int defaultChatPort=55555;
    private static String defaultServer="localhost";

    public static void main(String[] args){
        try {
            Properties clientProps=new Properties();
            try {
                clientProps.load(StartRpcClient.class.getResourceAsStream("/my_client.properties"));
                System.out.println("Client properties set. ");
                clientProps.list(System.out);
            } catch (IOException e) {
                System.err.println("Cannot find chatclient.properties "+e);
                return;
            }
            String serverIP=clientProps.getProperty("my_server.host",defaultServer);
            int serverPort=defaultChatPort;
            try{
                serverPort=Integer.parseInt(clientProps.getProperty("my_server.port"));
            }catch(NumberFormatException ex){
                System.err.println("Wrong port number "+ex.getMessage());
                System.out.println("Using default port: "+defaultChatPort);
            }
            System.out.println("Using server IP: " + serverIP);
            System.out.println("Using server port: " + serverPort);

            IServerService service = new ServerRpcProxy(serverIP, serverPort);
            _controller = new ControllerDonatii(service);
            launch(args);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
/*
    @Override
    public void stop(){
        System.out.println("Client closed the window");
        // Save file
    }
*/
    @Override
    public void start(Stage primaryStage) throws Exception {
        setPrimaryStage(primaryStage);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("Login.fxml"));
        Pane myPane = (Pane) loader.load();
        LoginWindow ctrl = loader.getController();

        //get service from xml file
        //ApplicationContext context1 = new ClassPathXmlApplicationContext("StartService.xml");
        ctrl.addController(this._controller);

        Scene myScene = new Scene(myPane);
        primaryStage.setScene(myScene);
        primaryStage.show();
    }

    public static Stage getPrimaryStage() {
        return pStage;
    }

    private void setPrimaryStage(Stage pStage) {
        this.pStage = pStage;
    }
}