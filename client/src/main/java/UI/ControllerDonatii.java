package UI;

import Domain.CazCaritabil;
import Domain.Donator;
import Domain.Voluntar;
import Service.IClientService;
import Service.IServerService;
import Service.ServiceException;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by sebi on 3/31/2017.
 */
public class ControllerDonatii extends Observable implements IClientService {

    private IServerService server;
    private Voluntar user;

    private Iterable<CazCaritabil> cazCaritabils;
    private Iterable<Donator> donators;

    public ControllerDonatii(IServerService service){
        this.server = service;
    }

    @Override
    public void updateDonatii(CazCaritabil cazCaritabil) throws ServiceException {
        System.out.println("cazul a ajuns la client in ControllerDonatii: " + cazCaritabil);
        setChanged();
        notifyObservers();
        /*for (CazCaritabil caz: cazCaritabils){
            if (caz.getId()==cazCaritabil.getId()){
                caz.setSumaTotala(cazCaritabil.getSumaTotala());
            }
        }*/
    }

    public boolean login(String username, String password) throws ServiceException {
        Voluntar userL= new Voluntar(username, password);
        user = userL;
        if (server.login(userL,this)){
            return true;
        }
        return false;
    }

    public Iterable<CazCaritabil> getAllCazCaritabils() throws ServiceException{
        return server.getAllCazCaritabils();
    }

    public void makeDonation(CazCaritabil caz, Donator donator, double sumaTotala) throws ServiceException{
        server.makeDonation(caz,donator,sumaTotala);
    }

    public void logOut() throws ServiceException{
        server.logOut(user, this);
    }

    public Iterable<Donator> getFilteredDonors(String startLetters) throws ServiceException {
        return server.getFilteredDonors(startLetters);
    }
    public Iterable<CazCaritabil> getCases() throws ServiceException {
        return cazCaritabils;
    }

   /* @Override
    public synchronized void addObserver(Observer o) {
        super.addObserver(o);
        System.out.println("addObsss");
    }*/
}
