package UI;


import Domain.CazCaritabil;
import Domain.Donator;
import Service.ServiceException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

/**
 * Created by sebi on 3/20/2017.
 */
public class MainWindow implements Initializable, Observer {
    private ControllerDonatii _controller;
    @FXML
    TextField txtName, txtSum, txtAdress, txtNoPhone;

    @FXML
    Button btnLogOut;

    @FXML
    private TableView<CazCaritabil> tblCazCaritabil;

    @FXML
    private TableView<Donator> tblDonatori;

    public void setController(ControllerDonatii controller) {
        this._controller = controller;
        _controller.addObserver(this);
        System.out.println("Observer adaugat");
        initData();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //autofill selected Donator data
        tblDonatori.getSelectionModel().selectedItemProperty().addListener((observable,oldvalue,newValue)->showDonatorDetails(newValue) );
        txtName.setOnKeyReleased(keyEvent -> initDonatorsTableData());

        try{Class myClass = Class.forName("StartRpcClient");
            Method myMethod = myClass.getMethod("getPrimaryStage");
            Stage primaryStage = (Stage) myMethod.invoke(null);
            primaryStage.setOnCloseRequest(e -> {
                try {
                    this._controller.logOut();
                    Platform.exit();
                }catch (ServiceException ex){
                    this._warning("Eroare la log out: " + ex.getMessage());
                }
            });

        }catch (ClassNotFoundException ex){
            this._warning("Nu s-a gasit clasa ceruta" + ex.getMessage());
        }catch (NoSuchMethodException ex){
            this._warning("Nu a gasit metoda " + ex.getMessage());
        }catch (InvocationTargetException ex) {
            this._warning("Eroare la executarea metodei" + ex.getMessage());
        }catch(IllegalAccessException ex){
            this._warning("Nu s-a reusit exexutarea metodei prin reflexie" + ex.getMessage());
        }
    }

    private void showDonatorDetails(Donator value){
        if (value==null)
            clearFields();
        else{
            txtName.setText(value.getNume());
            txtAdress.setText(value.getAdresa());
            txtNoPhone.setText(value.getNrTelefon());
        }
    }
    @FXML private void clearFields(){
        txtName.setText("");
        txtNoPhone.setText("");
        txtAdress.setText("");
    }

    private void initDonatorsTableData(){
        tblDonatori.getItems().clear();
        if(txtName.getText() == "" || txtName.getText() == null)
            return;
        try {
            for (Donator d : _controller.getFilteredDonors(txtName.getText())) {
                tblDonatori.getItems().add(d);
            }
        }catch (ServiceException ex){
            this._warning("Nu s-au putut obtine donatorii"  + ex.getMessage());
        }
    }
//
//    public void setService(ServiceDonatii service){
//
//        this._serviceDonatie = service;
//        initData();
//    }

    private void initData(){
        tblCazCaritabil.getItems().clear();
        try {
            for (CazCaritabil c : _controller.getAllCazCaritabils()) {
                tblCazCaritabil.getItems().add(c);
            }

        }catch (ServiceException ex){
            this._warning("Nu a reusit preluarea datelor despre cazuri" + ex.getMessage());
        }
    }

    @FXML
    private void onDonate(){
        String numeDonator = txtName.getText();
        String address = txtAdress.getText();
        String phone = txtNoPhone.getText();
        double sumaTotala = 0;
        try {
            sumaTotala = Double.parseDouble(txtSum.getText());
        }
        catch (NumberFormatException ex){
            this._warning("Suma trebuie sa fie numar pozitiv! \n" + ex.getMessage());
        }
        try {
            CazCaritabil caz = tblCazCaritabil.getSelectionModel().getSelectedItem();
            if(caz == null){
                throw new Exception("Selectati cazul caritabil!");
            }
            Donator donator = new Donator(0,numeDonator,address,phone);
            this._controller.makeDonation(caz, donator, sumaTotala);
           // this.initData();
        }
        catch (Exception e){
            this._warning("Nu a reusit donatia "+e.getMessage());
        }
    }

/*
    @FXML
    public void exitApplication(ActionEvent event) {
        Platform.exit();
    }
*/
    @FXML
    private void onLogOut(){

        try {
            //use reflection for showing the LoginWindow form again
            Class myClass = Class.forName("StartRpcClient");
            Method myMethod = myClass.getMethod("getPrimaryStage");
            Stage primaryStage = (Stage) myMethod.invoke(null);

            primaryStage.show();
            _controller.logOut();
        }
        catch (Exception ex){
            this._warning("Log out exception: " + ex.getMessage());
        }

        Stage stage = (Stage) btnLogOut.getScene().getWindow();
        stage.hide();
    }

    protected void _warning(String message)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(message);
        alert.showAndWait();
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Update main");
        Platform.runLater( ()->initData() );
    }
}
