package UI;//import Controllers.RefereeController;
//import JDBC.Helper;

import Service.ServiceException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class LoginWindow
{
    private ControllerDonatii _controller;

    @FXML
    private AnchorPane _root;
    @FXML
    private TextField _username;
    @FXML
    private PasswordField _password;

    public LoginWindow(){}

    public void addController(ControllerDonatii controller){
        //this._service = context.getBean(ServiceDonatii.class);
        this._controller = controller;
    }

    /**
     * Pops up an error alert
     *
     * @param message String
     */
    protected void _warning(String message)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(message);
        alert.showAndWait();
    }

    /**
     * Checks if the loin data is correct and acts accordingly
     *
     * @param actionEvent ActionEvent
     */
    @FXML
    private void onLoginAttempt(ActionEvent actionEvent)
    {
        String username = this._username.getText();
        String password = this._password.getText();

        try {
            if (this._controller.login(username, password)) {
                this._successLogin();
            } else {
                this._warning("Incorrect login data");
            }
        }catch (ServiceException ex){
            this._warning( "Incorrect data: " + ex.getMessage());
        }
    }

    private void _successLogin()
    {
        try {
            Stage stage = (Stage) this._root.getScene().getWindow();
            //stage.setResizable(false);
            stage.hide();
            this._password.setText("");
            this._username.setText("");


            Stage stage1 = new Stage();
            stage1.setResizable(false); //
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/MainWindow.fxml"));
            Pane myPane = (Pane) loader.load();
            MainWindow mainWindow = loader.getController();

//            CazCaritabilJdbcRepository cazCaritabilJdbcRepository = new CazCaritabilJdbcRepository(this._service.getProperties());
//            DonatorJdbcRepository donatorJdbcRepository = new DonatorJdbcRepository(this._service.getProperties());
//            ctrl.setService(getServiceDonatie(cazCaritabilJdbcRepository, donatorJdbcRepository), getCazService(cazCaritabilJdbcRepository), getDonatorService(donatorJdbcRepository));

            //ApplicationContext context2 = new ClassPathXmlApplicationContext("StartService.xml");
            mainWindow.setController(this._controller);
            Scene myScene = new Scene(myPane);

            stage1.setScene(myScene);
            stage1.show();


        } catch (Exception e) {
            this._warning(e.getMessage());
            e.printStackTrace();
        }
    }

    /*
    private ServiceDonatii getServiceDonatie(CazCaritabilJdbcRepository cazCaritabilJdbcRepository, DonatorJdbcRepository donatorJdbcRepository){
        return new ServiceDonatii(new DonatieJdbcRepository(cazCaritabilJdbcRepository,donatorJdbcRepository, this._service.getProperties()));
    }
    private ServiceCazCaritabil getCazService(CazCaritabilJdbcRepository repo){
        return new ServiceCazCaritabil(repo);
    }
    private ServiceDonator getDonatorService(DonatorJdbcRepository repository){return new ServiceDonator(repository);}
    */
}