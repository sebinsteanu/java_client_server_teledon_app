package Service;

/**
 * Created by sebi on 3/29/2017.
 */
public class ServiceException extends Exception{
    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
            super(message, cause);
        }

}
