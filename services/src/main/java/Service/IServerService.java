package Service;

import Domain.CazCaritabil;
import Domain.Donator;
import Domain.Voluntar;

/**
 * Created by sebi on 3/28/2017.
 */
public interface IServerService {
    void makeDonation(CazCaritabil cazCaritabil, Donator donator, double suma) throws ServiceException;
    Iterable<Donator> getFilteredDonors(String startLetters) throws ServiceException;             //filtered donors
    Iterable<CazCaritabil> getAllCazCaritabils() throws ServiceException;
    boolean login(Voluntar voluntar, IClientService clientService) throws ServiceException;
    void logOut(Voluntar user, IClientService clientService) throws ServiceException;
}
