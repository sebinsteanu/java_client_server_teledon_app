package Service;

import Domain.CazCaritabil;

/**
 * Created by sebi on 3/29/2017.
 */
public interface IClientService {
    void updateDonatii(CazCaritabil cazCaritabil)  throws ServiceException;
}
