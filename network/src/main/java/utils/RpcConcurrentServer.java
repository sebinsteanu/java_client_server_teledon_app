package utils;

import Service.IServerService;
import rpcProtocol.ClientRpcWorker;

import java.net.Socket;

/**
 * Created by sebi on 3/28/2017.
 */
public class RpcConcurrentServer extends AbsConcurrentServer {
    private IServerService donServer;
    public RpcConcurrentServer(int port, IServerService donServer) {
        super(port);
        this.donServer = donServer;
        System.out.println("Initializing - RpcConcurrentServer");
    }

    @Override
    protected Thread createWorker(Socket clientSocket) {
        ClientRpcWorker worker = new ClientRpcWorker(donServer, clientSocket);
        Thread tw=new Thread(worker);
        return tw;
    }


}
