package utils;

import java.net.Socket;

/**
 * Created by sebi on 3/28/2017.
 */
public abstract class AbsConcurrentServer extends AbstractServer {

    public AbsConcurrentServer(int port) {
        super(port);
        System.out.println("Initializing - Concurrent AbstractServer");
    }

    protected void processRequest(Socket clientSocket) {
        Thread tw = createWorker(clientSocket);
        tw.start();
    }

    protected abstract Thread createWorker(Socket clientSocket) ;
}
