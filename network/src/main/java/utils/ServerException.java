package utils;

/**
 * Created by sebi on 3/28/2017.
 */
public class ServerException extends Exception {
    public ServerException(String ex) {super(ex);}

    public ServerException(String message, Throwable cause) {
        super(message, cause);
    }
}
