package rpcProtocol;

import Domain.CazCaritabil;
import Domain.Donator;
import Domain.Voluntar;
import Service.IClientService;
import Service.IServerService;
import Service.ServiceException;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by sebi on 3/31/2017.
 */
public class ClientRpcWorker implements Runnable, IClientService {
    private IServerService server;                      //StartServerServiceImpl
    private Socket connection;

    private ObjectInputStream input;
    private ObjectOutputStream output;
    private volatile boolean connected;

    public ClientRpcWorker(IServerService server, Socket connection) {
        this.server = server;
        this.connection = connection;
        try{
            output=new ObjectOutputStream(connection.getOutputStream());
            output.flush();
            input=new ObjectInputStream(connection.getInputStream());
            connected=true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateDonatii(CazCaritabil cazCaritabil) throws ServiceException{
        Response resp=new Response.Builder().type(ResponseType.UPDATE_CASES).build();
        System.out.println("Made update on case:  "+ cazCaritabil);
        try {
            sendResponse(resp);
        } catch (IOException e) {
            throw new ServiceException("Sending error: "+e);
        }
    }

    @Override
    public void run() {
        while(connected){
            try {
                Object request=input.readObject();
                Response response = handleRequest((Request)request);
                if (response!=null){
                    sendResponse(response);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            input.close();
            output.close();
            connection.close();
        } catch (IOException e) {
            System.out.println("Error "+e);
        }

    }


    private static Response okResponse=new Response.Builder().type(ResponseType.OK).build();

    private Response handleRequest(Request request){
        Response response = null;
        if (request.type() == RequestType.LOGIN){
            System.out.println("Login request ..."+request.type());
            Voluntar user=(Voluntar)request.data();
            try {
                server.login(user, this);
                return okResponse;
            } catch (ServiceException e) {
                connected=false;
                return new Response.Builder().type(ResponseType.ERROR).data(e.getMessage()).build();
            }
        }
        if (request.type()==RequestType.GET_ALL_CASES){
            System.out.println("Get cases request ...");
            try {
                Iterable<CazCaritabil> cases=server.getAllCazCaritabils();
                return new Response.Builder().type(ResponseType.GET_ALL_CASES).data(cases).build();
            } catch (ServiceException e) {
                return new Response.Builder().type(ResponseType.ERROR).data(e.getMessage()).build();
            }
        }

        if (request.type()==RequestType.LOGOUT){
            System.out.println("Logout request ...");
            Voluntar user=(Voluntar) request.data();
            try {
                server.logOut(user, this);
                connected = false;
                return okResponse;

            } catch (ServiceException e) {
                return new Response.Builder().type(ResponseType.ERROR).data(e.getMessage()).build();
            }
        }

        if (request.type() == RequestType.MAKE_DONATION){
            System.out.println("Make donation request ...");
            try {
                LinkedList<Object> my_arr = (LinkedList<Object>) request.data();
                CazCaritabil cazCaritabil = (CazCaritabil) my_arr.get(0);
                Donator donator = (Donator) my_arr.get(1);
                Double suma = (Double) my_arr.get(2);

                server.makeDonation(cazCaritabil, donator, suma);
                return new Response.Builder().type(ResponseType.OK).build();
            } catch (ServiceException e) {
                return new Response.Builder().type(ResponseType.ERROR).data(e.getMessage()).build();
            }

        }

        if (request.type() == RequestType.GET_FILTERED_DONORS){
            System.out.println("Get filtered donoors request ...");
            try {
                Iterable<Donator> cases=server.getFilteredDonors((String) request.data());
                return new Response.Builder().type(ResponseType.GET_FILTERED_DONORS).data(cases).build();
            } catch (ServiceException e) {
                return new Response.Builder().type(ResponseType.ERROR).data(e.getMessage()).build();
            }
        }
/*
        if (request.type()==RequestType.GET_LOGGED_FRIENDS){
            System.out.println("GetLoggedFriends Request ...");
            UserDTO udto=(UserDTO)request.data();
            User user=DTOUtils.getFromDTO(udto);
            try {
                User[] friends=server.getLoggedFriends(user);
                UserDTO[] frDTO=DTOUtils.getDTO(friends);
                return new Response.Builder().type(ResponseType.GET_LOGGED_FRIENDS).data(frDTO).build();
            } catch (ServiceException e) {
                return new Response.Builder().type(ResponseType.ERROR).data(e.getMessage()).build();
            }
        }*/
        return response;
    }

    private void sendResponse(Response response) throws IOException{
        System.out.println("sending response "+response);
        output.writeObject(response);
        output.flush();
    }
}
