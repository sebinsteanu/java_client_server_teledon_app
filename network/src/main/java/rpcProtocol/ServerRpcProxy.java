package rpcProtocol;

import Domain.CazCaritabil;
import Domain.Donator;
import Domain.Voluntar;
import Service.IClientService;
import Service.IServerService;
import Service.ServiceException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by sebi on 3/31/2017.
 */
public class ServerRpcProxy implements IServerService {
    private String host;
    private int port;

    private IClientService client;

    private ObjectInputStream input;
    private ObjectOutputStream output;
    private Socket connectionSocket;

    private BlockingQueue<Response> qresponses;
    private volatile boolean finished;


    public ServerRpcProxy(String host, int port) {
        this.host = host;
        this.port = port;
        qresponses=new LinkedBlockingQueue<Response>();
    }


    @Override
    public Iterable<Donator> getFilteredDonors(String startLetters) throws ServiceException {
        Request request = new Request.Builder().type(RequestType.GET_FILTERED_DONORS).data(startLetters).build();
        sendRequest(request);
        Response response = readResponse();

        if (response.type()== ResponseType.ERROR){
            String err=response.data().toString();
            throw new ServiceException(err);
        }
        Iterable<Donator> donators = (Iterable<Donator>) response.data();
        return donators;
    }
/*
    @Override
    public Iterable<Donator> getFilteredDonors() throws ServiceException {
        return null;
    }*/

    @Override
    public Iterable<CazCaritabil> getAllCazCaritabils() throws ServiceException{
        Request req = new Request.Builder().type(RequestType.GET_ALL_CASES).build();
        sendRequest(req);
        Response response=readResponse();
        if (response.type()== ResponseType.ERROR){
            String err=response.data().toString();
            throw new ServiceException(err);
        }
        Iterable<CazCaritabil> cazCaritabils = (Iterable<CazCaritabil>) response.data();
        return cazCaritabils;
    }

    @Override
    public boolean login(Voluntar voluntar, IClientService clientService) throws ServiceException {
        initializeConnection();
        Request req=new Request.Builder().type(RequestType.LOGIN).data(voluntar).build();
        sendRequest(req);
        Response response = readResponse();
        if (response.type()== ResponseType.OK){
            this.client = clientService;
            return true;
        }
        if (response.type() == ResponseType.ERROR){
            String err=response.data().toString();
            closeConnection();
            throw new ServiceException(err);
        }
        return false;
    }

    @Override
    public void logOut(Voluntar user, IClientService clientService) throws ServiceException {
        Request req = new Request.Builder().type(RequestType.LOGOUT).data(user).build();
        sendRequest(req);
        Response response = readResponse();
        if(response.type() == ResponseType.OK){
            closeConnection();
        }
        else {
            String err = response.data().toString();
            throw new ServiceException(err);
        }
    }
    private void startReader(){
        Thread tw=new Thread(new ReaderThread());
        tw.start();
    }

    @Override
    public void makeDonation(CazCaritabil cazCaritabil, Donator donator, double suma) throws ServiceException {
        LinkedList<Object> my_arr = new LinkedList<>();
        my_arr.add(cazCaritabil);
        my_arr.add(donator);
        my_arr.add(suma);
        Request request = new Request.Builder().type(RequestType.MAKE_DONATION).data(my_arr).build();
        sendRequest(request);
        Response response = readResponse();
        if(response.type() == ResponseType.OK){

        }
    }

    private void sendRequest(Request request)throws ServiceException {
        try {
            output.writeObject(request);
            output.flush();
        } catch (IOException e) {
            throw new ServiceException("Error sending object "+e);
        }
    }
    private Response readResponse() throws ServiceException {
        Response response=null;
        try{
            /*synchronized (responses){
                responses.wait();
            }
            response = responses.remove(0);    */
            response=qresponses.take();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response;
    }

    private void handleUpdate(Response response){
        CazCaritabil cazCaritabil = (CazCaritabil) response.data();
        System.out.println("S-a adaugat o donatie pentru cazul: \"" + cazCaritabil + "\"");
        try {
            client.updateDonatii(cazCaritabil);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }

    private class ReaderThread implements Runnable{
        public void run() {
            while(!finished){
                try {
                    Response response = (Response) input.readObject();
                    System.out.println("Response received: " + response);
                    if ((response).type() == ResponseType.UPDATE_CASES){
                        handleUpdate(response);
                    }else{

                        try {
                            qresponses.put((Response)response);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IOException e) {
                    System.out.println("Reading error "+e);
                } catch (ClassNotFoundException e) {
                    System.out.println("Reading error "+e);
                }
            }
        }
    }
    private void initializeConnection() throws ServiceException {
        try {
            connectionSocket=new Socket(host,port);
            output=new ObjectOutputStream(connectionSocket.getOutputStream());
            output.flush();
            input=new ObjectInputStream(connectionSocket.getInputStream());
            finished=false;
            startReader();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void closeConnection() {
        finished=true;
        try {
            input.close();
            output.close();
            connectionSocket.close();
            client=null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
