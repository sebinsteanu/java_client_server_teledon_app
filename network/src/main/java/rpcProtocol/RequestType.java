package rpcProtocol;

/**
 * Created by sebi on 3/31/2017.
 */
public enum RequestType {
    LOGIN, LOGOUT, GET_FILTERED_DONORS, GET_ALL_CASES, MAKE_DONATION;
}
