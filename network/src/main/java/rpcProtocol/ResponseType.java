package rpcProtocol;

/**
 * Created by sebi on 3/31/2017.
 */
public enum ResponseType {
    OK, ERROR, UPDATE_CASES, GET_FILTERED_DONORS, GET_ALL_CASES, LOG_OUT;
}