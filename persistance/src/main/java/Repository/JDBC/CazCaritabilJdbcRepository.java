package Repository.JDBC;

import Domain.CazCaritabil;
import Repository.IRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by sebi on 3/12/2017.
 */
public class CazCaritabilJdbcRepository implements IRepository<Integer, CazCaritabil> {
    private JdbcUtils dbUtils;
    private static Connection _connection;

    public CazCaritabilJdbcRepository(Properties props) {
        dbUtils = new JdbcUtils(props);
        _connection = dbUtils.getConnection();
    }

    @Override
    public int size() {
        try (PreparedStatement preStmt = this._connection.prepareStatement("select count(*) as [SIZE] from CazCaritabil")) {
            try (ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    return result.getInt("SIZE");
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
        return 0;
    }

    @Override
    public void save(CazCaritabil entity) {
        try (PreparedStatement preStmt = this._connection.prepareStatement("insert into CazCaritabil values (SELECT MAX(id) + 1 FROM CazCaritabil),?,?)")) {
            //preStmt.setInt(1, entity.getId());
            preStmt.setString(1, entity.getNumeCaz());
            preStmt.setDouble(2, entity.getSumaTotala());

            int result = preStmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
    }

    @Override
    public void delete(Integer integer) {
        try (PreparedStatement preStmt = this._connection.prepareStatement("delete from CazCaritabil where id=?")) {
            preStmt.setInt(1, integer);

            int result = preStmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
    }

    @Override
    public void update(Integer integer, CazCaritabil entity) {
        try(PreparedStatement stmt = this._connection.prepareStatement("UPDATE CazCaritabil SET id=?,numeCaz=?,sumaTotala=? WHERE id=?")){
            stmt.setInt(1,entity.getId());
            stmt.setString(2,entity.getNumeCaz());
            stmt.setDouble(3,entity.getSumaTotala());
            stmt.setInt(4,integer);

            int result = stmt.executeUpdate();
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public CazCaritabil findOne(Integer integer) {
        try (PreparedStatement preStmt = this._connection.prepareStatement("select * from CazCaritabil where id=?")) {
            preStmt.setInt(1, integer);
            try (ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    int id = result.getInt("id");
                    String numeCaz = result.getString("numeCaz");
                    double sumaTotala = result.getDouble("sumaTotala");
                    CazCaritabil cazCaritabil = new CazCaritabil(id, numeCaz, sumaTotala);
                    return cazCaritabil;
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
        return null;
    }

    @Override
    public Iterable<CazCaritabil> findAll() {
        List<CazCaritabil> cazuri = new ArrayList<>();
        try (PreparedStatement preStmt = this._connection.prepareStatement("select * from CazCaritabil")) {
            try (ResultSet result = preStmt.executeQuery()) {
                while (result.next()) {
                    int id = result.getInt("id");
                    String numeCaz = result.getString("numeCaz");
                    double sumaTotala = result.getDouble("sumaTotala");
                    CazCaritabil caz = new CazCaritabil(id, numeCaz, sumaTotala);
                    cazuri.add(caz);
                }
            }
        } catch (SQLException e) {
            System.out.println("Error DB " + e);
        }
        return cazuri;
    }
}
