package Repository.JDBC;

import Domain.Donator;
import Repository.IRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by sebi on 3/12/2017.
 */
public class DonatorJdbcRepository implements IRepository<Integer, Donator> {
    private JdbcUtils dbUtils;
    private static Connection _connection;

    public DonatorJdbcRepository(Properties props) {
        dbUtils = new JdbcUtils(props);
        this._connection = dbUtils.getConnection();
    }
    @Override
    public int size() {
        try (PreparedStatement preStmt = this._connection.prepareStatement("select count(*) as [SIZE] from Donator")) {
            try (ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    return result.getInt("SIZE");
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
        return 0;
    }

    @Override
    public void save(Donator entity) {
        try (PreparedStatement preStmt = this._connection.prepareStatement("insert into Donator values ((SELECT MAX(id) + 1 FROM Donator),?,?,?)")) {
            //preStmt.setInt(1, entity.getId());
            preStmt.setString(1, entity.getNume());
            preStmt.setString(2, entity.getAdresa());
            preStmt.setString(3, entity.getNrTelefon());

            int result = preStmt.executeUpdate();
            //super.save(entity);
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
    }

    @Override
    public void delete(Integer integer) {
        try (PreparedStatement preStmt = this._connection.prepareStatement("delete from Donator where id=?")) {
            preStmt.setInt(1, integer);

            int result = preStmt.executeUpdate();
            //super.delete(integer);
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
    }

    @Override
    public void update(Integer integer, Donator entity) {
        try(PreparedStatement stmt = this._connection.prepareStatement("UPDATE Donator SET id=?,nume=?,adresa=?, nrTelefon=? WHERE id=?")){
            stmt.setInt(1,entity.getId());
            stmt.setString(2,entity.getNume());
            stmt.setString(3,entity.getAdresa());
            stmt.setString(4,entity.getNrTelefon());
            stmt.setInt(5,integer);

            int result = stmt.executeUpdate();
            //super.update(integer, entity);
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public Donator findOne(Integer integer) {

        try (PreparedStatement preStmt = this._connection.prepareStatement("select * from Donator where id=?")) {
            preStmt.setInt(1, integer);
            try (ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    int id = result.getInt("id");
                    String nume = result.getString("nume");
                    String adresa = result.getString("adresa");
                    String nrTelefon = result.getString("nrTelefon");
                    Donator donator = new Donator(id, nume, adresa,nrTelefon );
                    return donator;
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
        return null;
    }

    @Override
    public Iterable<Donator> findAll() {
        List<Donator> donators = new ArrayList<>();
        try (PreparedStatement preStmt = this._connection.prepareStatement("select * from Donator")) {
            try (ResultSet result = preStmt.executeQuery()) {
                while (result.next()) {
                    donators.add(getDonatorFromResult(result));
                }
            }
        } catch (SQLException e) {
            System.out.println("Error DB " + e);
        }
        return donators;
    }

    public Iterable<Donator> filtered(String start) {
        List<Donator> donators = new ArrayList<>();
        try (PreparedStatement preStmt = this._connection.prepareStatement("select * from Donator WHERE nume LIKE ?")) {
            start = start + "%";
            preStmt.setString(1, start);
            try (ResultSet result = preStmt.executeQuery()) {
                while (result.next()) {
                    donators.add(getDonatorFromResult(result));
                }
            }
        } catch (SQLException e) {
            System.out.println("Error DB " + e);
        }
        return donators;
    }
    private static Donator getDonatorFromResult(ResultSet result) throws SQLException{
        int id = result.getInt("id");
        String nume = result.getString("nume");
        String adresa = result.getString("adresa");
        String nrTelefon = result.getString("nrTelefon");
        return new Donator(id, nume, adresa, nrTelefon);
    }
}
