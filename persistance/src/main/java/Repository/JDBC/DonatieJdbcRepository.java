package Repository.JDBC;

import Domain.CazCaritabil;
import Domain.Donatie;
import Domain.Donator;
import Repository.IDonatieRepo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by sebi on 3/8/2017.
 */
public class DonatieJdbcRepository implements IDonatieRepo<Integer, Donatie> {
    private JdbcUtils dbUtils;
    private CazCaritabilJdbcRepository cazCaritabilJdbcRepository;
    private DonatorJdbcRepository donatorJdbcRepository;
    private static Connection _connection;

    public DonatieJdbcRepository(CazCaritabilJdbcRepository cazCaritabilJdbcRepository, DonatorJdbcRepository donatorJdbcRepository, Properties props) {
        this.cazCaritabilJdbcRepository = cazCaritabilJdbcRepository;
        this.donatorJdbcRepository = donatorJdbcRepository;
        dbUtils = new JdbcUtils(props);
        this._connection = dbUtils.getConnection();
    }

    @Override
    public int size() {
        try (PreparedStatement preStmt = _connection.prepareStatement("select count(*) as [SIZE] from Donatie")) {
            try (ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    return result.getInt("SIZE");
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
        return 0;
    }

    @Override
    public void save(Donatie entity) {
        try (PreparedStatement preStmt = _connection.prepareStatement("insert into Donatie values ((SELECT ISNULL(MAX(id) + 1, 1) FROM Donatie),?,?,?)")) {
            //preStmt.setInt(1, entity.getId());
            preStmt.setDouble(1, entity.getsuma());
            preStmt.setInt(2, entity.getidDonator());
            preStmt.setInt(3,entity.getidCazCaritabil());

            CazCaritabil cazCaritabil = cazCaritabilJdbcRepository.findOne(entity.getidCazCaritabil());
            if(cazCaritabil!=null) {
                cazCaritabil.setSumaTotala(cazCaritabil.getSumaTotala() + entity.getsuma());
                cazCaritabilJdbcRepository.update(cazCaritabil.getId(), cazCaritabil);
            }

            int result = preStmt.executeUpdate();
            //super.save(entity);
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
    }

    public CazCaritabil addDonation(CazCaritabil cazCaritabilD, Donator donator, double suma){

        try (PreparedStatement preStmt = _connection.prepareStatement("insert into Donatie values ((SELECT MAX(id) + 1 FROM Donatie),?,?,?)")) {
            int idDonator = 0 ;
            for(Donator donator1: donatorJdbcRepository.findAll()){
                if(donator1.equals(donator)) {
                    idDonator = donator1.getId();
                    donator.setId(idDonator);
                    donatorJdbcRepository.update(idDonator, donator);
                }
            }
            if(idDonator ==0) {
                donatorJdbcRepository.save(donator);
                try (PreparedStatement preStmt2 = _connection.prepareStatement("select MAX(id) as [ID] from Donator")) {
                    try (ResultSet result = preStmt2.executeQuery()) {
                        if (result.next()) {
                            idDonator = result.getInt("ID");
                        }
                    }
                } catch (SQLException ex) {
                    System.out.println("Error DB " + ex);
                }
            }

            CazCaritabil cazCaritabil = cazCaritabilJdbcRepository.findOne(cazCaritabilD.getId());
            if(cazCaritabil!=null) {
                cazCaritabil.setSumaTotala(cazCaritabil.getSumaTotala() + suma);
                cazCaritabilJdbcRepository.update(cazCaritabil.getId(), cazCaritabil);
            }
            else {
                cazCaritabilJdbcRepository.save(cazCaritabil);
            }
            //preStmt.setInt(1, entity.getId());
            preStmt.setDouble(1, suma);
            preStmt.setInt(2, idDonator);
            preStmt.setInt(3,cazCaritabil.getId());

            int result = preStmt.executeUpdate();
            return cazCaritabil;
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
        return null;
    }

    @Override
    public void delete(Integer integer) {
        try (PreparedStatement preStmt = _connection.prepareStatement("delete from Donatie where id=?")) {
            preStmt.setInt(1, integer);

            int result = preStmt.executeUpdate();
            //super.delete(integer);
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
    }

    @Override
    public void update(Integer integer, Donatie entity) {
        try(PreparedStatement stmt = _connection.prepareStatement("UPDATE Donatie SET id=?,suma=?,idDonator=?,idCazCaritabil=? WHERE id=?")){
            stmt.setInt(1,entity.getId());
            stmt.setDouble(2,entity.getsuma());
            stmt.setInt(3,entity.getidDonator());
            stmt.setInt(4,entity.getidCazCaritabil());
            stmt.setInt(5,integer);

            int result = stmt.executeUpdate();
            //super.update(integer,entity);
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public Donatie findOne(Integer integer) {

        try (PreparedStatement preStmt = _connection.prepareStatement("select * from Donatie where id=?")) {
            preStmt.setInt(1, integer);
            try (ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    int id = result.getInt("id");
                    double suma = result.getDouble("suma");
                    int idDonator = result.getInt("idDonator");
                    int idCazCaritabil = result.getInt("idCazCaritabil");
                    Donatie don = new Donatie(id, suma, idDonator, idCazCaritabil);
                    return don;
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
        return null;
    }

    @Override
    public Iterable<Donatie> findAll() {
        List<Donatie> donatii = new ArrayList<>();
        try (PreparedStatement preStmt = _connection.prepareStatement("select * from Donatie")) {
            try (ResultSet result = preStmt.executeQuery()) {
                while (result.next()) {
                    int id = result.getInt("id");
                    double suma = result.getDouble("suma");
                    int idDonator = result.getInt("idDonator");
                    int idCazCaritabil = result.getInt("idCazCaritabil");
                    Donatie don = new Donatie(id, suma, idDonator, idCazCaritabil);
                    donatii.add(don);
                }
                return donatii;
            }
        } catch (SQLException e) {
            System.out.println("Error DB " + e);
        }
        return null;
    }

    public Iterable<Donator> findDonators(String start){
        return donatorJdbcRepository.filtered(start);
    }
    public Iterable<CazCaritabil> findAllCharityCases(){
        return cazCaritabilJdbcRepository.findAll();
    }
}
