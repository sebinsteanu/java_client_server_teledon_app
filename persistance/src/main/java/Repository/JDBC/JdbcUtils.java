package Repository.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by sebi on 3/8/2017.
 */
public class JdbcUtils {
    private Properties jdbcProps;

    public JdbcUtils(Properties props){
        jdbcProps=props;
    }
    private Connection instance=null;
    private Connection getNewConnection(){
        String driver=jdbcProps.getProperty("sebi.jdbc.driver");
        String url=jdbcProps.getProperty("sebi.jdbc.url");
        String user=jdbcProps.getProperty("sebi.jdbc.user");
        String pass=jdbcProps.getProperty("sebi.jdbc.pass");
        Connection con=null;
        try {
            Class.forName(driver);
            if (user!=null && pass!=null)
                con= DriverManager.getConnection(url,user,pass);
            else
                con=DriverManager.getConnection(url);
        } catch (ClassNotFoundException e) {
            System.out.println("Error loading driver "+e);
        } catch (SQLException e) {
            System.out.println("Error getting connection "+e);
        }
        return con;
    }

    public Connection getConnection(){
        try {
            if (instance==null || instance.isClosed())
                instance=getNewConnection();

        } catch (SQLException e) {
            System.out.println("Error DB "+e);
        }
        return instance;
    }
}
