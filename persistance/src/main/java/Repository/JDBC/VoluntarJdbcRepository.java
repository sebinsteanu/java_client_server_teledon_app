package Repository.JDBC;

import Domain.Voluntar;
import Repository.IRepository;
import Repository.IVoluntarRepo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by sebi on 3/10/2017.
 */
public class VoluntarJdbcRepository implements IVoluntarRepo{
    private JdbcUtils dbUtils;
    private static Connection _connection;

    public VoluntarJdbcRepository(Properties props) {
        this.dbUtils = new JdbcUtils(props);
        this._connection = dbUtils.getConnection();
    }

    @Override
    public int size() {
        try (PreparedStatement preStmt = this._connection.prepareStatement("select count(*) as [SIZE] from Voluntar")) {
            try (ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    return result.getInt("SIZE");
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
        return 0;
    }

    @Override
    public void save(Voluntar entity) {
        //try (PreparedStatement preStmt = con.prepareStatement("insert into Voluntar values (?,?,?)")) {
        try (PreparedStatement preStmt = this._connection.prepareStatement("insert into Voluntar values (?,?)")) {
            //preStmt.setInt(1, entity.getId());
            preStmt.setString(1, entity.getUsername());
            preStmt.setString(2, entity.getParola());

            int result = preStmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
    }

    @Override
    public void delete(String s) {
        try (PreparedStatement preStmt = this._connection.prepareStatement("delete from Voluntar where username=?")) {
            preStmt.setString(1, s);

            int result = preStmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
    }

    @Override
    public void update(String s, Voluntar entity) {
        try(PreparedStatement stmt = this._connection.prepareStatement("UPDATE Voluntar SET id=?,username=?,parola=? WHERE username=?")){
            stmt.setInt(1,entity.getId());
            stmt.setString(2,entity.getUsername());
            stmt.setString(3,entity.getParola());
            stmt.setString(4,s);

            int result = stmt.executeUpdate();
            stmt.close();
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public Voluntar findOne(String s) {
        try (PreparedStatement preStmt = this._connection.prepareStatement("select * from Voluntar where username=?")) {
            preStmt.setString(1, s);
            try (ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    int id = result.getInt("id");
                    String username = result.getString("username");
                    String parola = result.getString("parola");
                    Voluntar voluntar = new Voluntar(id, username, parola);
                    return voluntar;
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error DB " + ex);
        }
        return null;
    }

    @Override
    public Iterable<Voluntar> findAll() {
        List<Voluntar> voluntars = new ArrayList<>();
        try (PreparedStatement preStmt = this._connection.prepareStatement("select * from Voluntar")) {
            try (ResultSet result = preStmt.executeQuery()) {
                while (result.next()) {
                    int id =result.getInt("id");
                    String username = result.getString("username");
                    String parola = result.getString("parola");
                    Voluntar voluntar = new Voluntar(id, username, parola);
                    voluntars.add(voluntar);
                }
                return voluntars;
            }
        } catch (SQLException e) {
            System.out.println("Error DB " + e);
        }
        return null;
    }
}
