package Repository;

/**
 * Created by sebi on 3/19/2017.
 */
public class RepositoryException extends RuntimeException {
    public RepositoryException(String message) {
        super(message);
    }

    public RepositoryException(Exception ex) {
        super(ex);
    }
}
