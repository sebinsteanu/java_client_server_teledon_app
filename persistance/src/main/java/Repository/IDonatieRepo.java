package Repository;

import Domain.CazCaritabil;
import Domain.Donator;
import Repository.IRepository;

/**
 * Created by sebi on 3/27/2017.
 */
public interface IDonatieRepo<ID,T> extends IRepository<ID,T> {
    CazCaritabil addDonation(CazCaritabil cazCaritabil, Donator donator, double suma);
    Iterable<Donator> findDonators(String startLetter);
    Iterable<CazCaritabil> findAllCharityCases();
}
