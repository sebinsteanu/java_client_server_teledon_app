package Domain;

import java.io.Serializable;

/**
 * Created by sebi on 3/9/2017.
 */
public class CazCaritabil implements HasId<Integer>, Serializable {
    private int id;
    private double sumaTotala;
    private String numeCaz;

    public CazCaritabil(int id, String numeCaz, double sumaTotala) {
        this.id = id;
        this.sumaTotala = sumaTotala;
        this.numeCaz = numeCaz;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer integer) {
        this.id = integer;
    }

    public double getSumaTotala() {
        return sumaTotala;
    }

    public void setSumaTotala(double sumaTotala) {
        this.sumaTotala = sumaTotala;
    }

    public String getNumeCaz() {
        return numeCaz;
    }

    public void setNumeCaz(String numeCaz) {
        this.numeCaz = numeCaz;
    }

    public String toString(){
        return id+" "+numeCaz+" "+sumaTotala;
    }
}
