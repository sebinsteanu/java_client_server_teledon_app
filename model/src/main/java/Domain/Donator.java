package Domain;

import java.io.Serializable;

/**
 * Created by sebi on 3/9/2017.
 */
public class Donator implements HasId<Integer>, Serializable {
    private String nume, adresa, nrTelefon;
    private int id;

    public Donator(int id, String nume, String adresa, String nrTelefon) {
        this.id = id;
        this.nume = nume;
        this.adresa = adresa;
        this.nrTelefon = nrTelefon;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;

    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getNrTelefon() {
        return nrTelefon;
    }

    public void setNrTelefon(String nrTelefon) {
        this.nrTelefon = nrTelefon;
    }

    public boolean equals(Donator ot){return (this.getNume().equals(ot.getNume()) && (this.nrTelefon.equals(ot.getNrTelefon()))); }

    public String toString(){
        return id+" "+nume+" "+adresa+" "+nrTelefon;
    }
}
