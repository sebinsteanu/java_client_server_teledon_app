package Domain;

import java.io.Serializable;

/**
 * Created by sebi on 3/8/2017.
 */
public class Donatie implements HasId<Integer>, Serializable{
    private int id, idCazCaritabil, idDonator;
    private double suma;

    public Donatie(int id, double suma, int idDonator, int idCazCaritabil) {
        this.id = id;
        this.idCazCaritabil = idCazCaritabil;
        this.idDonator = idDonator;
        this.suma = suma;
    }

    public int getidDonator() {
        return idDonator;
    }

    public void setidDonator(int idDonatie) {
        this.idDonator = idDonatie;
    }

    public int getidCazCaritabil() {
        return idCazCaritabil;
    }

    public void setidCazCaritabil(int idCazCaritabil) {
        this.idCazCaritabil = idCazCaritabil;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer integer) {
        this.id = integer;
    }

    public double getsuma() {
        return suma;
    }

    public void setsuma(double suma) {
        this.suma = suma;
    }

    public String toString(){
        return id+" "+idDonator+" "+suma;
    }

}