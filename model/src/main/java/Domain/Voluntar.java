package Domain;

import java.io.Serializable;

/**
 * Created by sebi on 3/9/2017.
 */
public class Voluntar implements HasId<Integer>, Serializable {
    private int id;
    private String username, parola;

    public Voluntar(int id, String username, String parola) {
        this.id = id;
        this.username = username;
        this.parola = parola;
    }
    public Voluntar(String username, String parola) {
        this.id = 0;
        this.username = username;
        this.parola = parola;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer integer) {
        this.id = integer;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getParola() {
        return parola;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }

    public String toString(){
        return id+" "+username+" "+parola;
    }
}
