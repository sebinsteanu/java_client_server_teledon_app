package Domain;

/**
 * Created by sebi on 3/19/2017.
 */
public interface HasId<ID> {
    ID getId();
    void setId(ID id);
}