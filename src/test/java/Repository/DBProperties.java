package Repository;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by sebi on 3/13/2017.
 */
public class DBProperties {
    protected static Properties getProps(){
        Properties serverProps=new Properties();
        try {
            serverProps.load(new FileReader("db.config"));
            //System.setProperties(serverProps);

            //System.out.println("Properties set. ");
            //serverProps.list(System.out);
            return serverProps;
        } catch (IOException e) {
            System.out.println("Cannot find db.config "+e);
        }
        return null;
    }
}
